$(function(){
  $('#cep').on('focusout',function(){
    var cep = $('#cep').val();
    var apiUri = 'https://viacep.com.br/ws/'+cep+'/json/';
    $.getJSON(apiUri,function(json){
      $('#address').val(json.logradouro);
      $('#city').val(json.localidade);
      $('#uf').val(json.uf);
    })
    .error(function(){
      alert("CEP nao encontrado");
    });
  });
});
