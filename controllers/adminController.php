<?php

class adminController extends controller {

	public function index() {
		$admin = new Admin();
		if (isset($_POST['adminUsername']) && isset($_POST['adminPassword'])) {
			$admin->adminAuth($_POST['adminUsername'], $_POST['adminPassword']);
		}

		if (!empty($_SESSION['adminLoginAuth']) && isset($_SESSION['adminLoginAuth']) && $_SESSION['adminLoginAuth'] == true) {
			$this->loadAdminTemplate('admin_dashboard', $data = array());
		} else {
			$this->loadView('admin_login', $data = array());
		}

	}
	public function about() {
		if (!empty($_SESSION['adminLoginAuth']) && isset($_SESSION['adminLoginAuth']) && $_SESSION['adminLoginAuth'] == true) {
			$a = new About();
			if (isset($_POST['descricao']) && !empty($_POST['descricao'])) {
				$s->addAbout($_POST['titulo'], $_POST['descricao']);
			}

			$a = new About();
			$dados['about'] = $a->getAbout();
			$this->loadAdminTemplate('about', $dados);
		} else {
			$this->loadView('admin_login', $data = array());
		}
	}

	public function users() {
		$dados = array();
		//REVER A SEGURANCA DESTA FUNCAO URGENTE!
		if (!empty($_SESSION['adminLoginAuth']) && isset($_SESSION['adminLoginAuth']) && $_SESSION['adminLoginAuth'] == true) {
			$u = new User();
			if (isset($_POST['email']) && !empty($_POST['email'])) {
				$cep = new Cep($_POST['cep']);
				$u->addUser($_POST['name'], $_POST['email'], $_POST['phone'], $_POST['cpf'], $_POST['cep'], $_POST['adress'], "", $_POST['city'], $_POST['uf'], $_POST['birthday'], $_POST['fantasy-name'], $_POST['social-reason']);
			}

			$u = new User();
			$dados['users'] = $u->getUsers();
			$this->loadAdminTemplate('user', $dados);
		} else {
			$this->loadView('admin_login', $data = array());
		}

	}

	public function services() {
		if (!empty($_SESSION['adminLoginAuth']) && isset($_SESSION['adminLoginAuth']) && $_SESSION['adminLoginAuth'] == true) {
			$s = new Services();
			if (isset($_POST['descricao']) && !empty($_POST['descricao'])) {
				$s->addService($_POST['titulo'], $_POST['descricao']);
			}

			$s = new Services();
			$dados['services'] = $s->getService();
			$this->loadAdminTemplate('services', $dados);
		} else {
			$this->loadView('admin_login', $data = array());
		}
	}

	public function projects() {
		$data = array();
		if (!empty($_SESSION['adminLoginAuth']) && isset($_SESSION['adminLoginAuth']) && $_SESSION['adminLoginAuth'] == true) {
			$p = new Projects();
			if (isset($_POST['descricao']) && !empty($_POST['descricao'])) {
				$s->addProject($_POST['titulo'], $_POST['descricao']);
			}

			$p = new Projects();
			$data['projects'] = $p->getProject();
			$this->loadAdminTemplate('projects', $data);
		} else {
			$this->loadView('admin_login', $data = array());
		}
	}

	public function logout() {
		$admin = new Admin();
		$admin->logout();
		header('Location:' . BASE_URL . 'admin');
	}
}
