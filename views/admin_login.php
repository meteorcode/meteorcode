
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dashboard MeteorCode</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon
    		============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo BASE_URL; ?>assets/admin/img/favicon.ico">
        <!-- Google Fonts
    		============================================ -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
        <!-- Bootstrap CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/bootstrap.min.css">
        <!-- Bootstrap CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/font-awesome.min.css">
        <!-- adminpro icon CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/adminpro-custon-icon.css">
        <!-- meanmenu icon CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/meanmenu.min.css">
        <!-- mCustomScrollbar CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/jquery.mCustomScrollbar.min.css">
        <!-- form CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/form.css">
        <!-- animate CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/animate.css">
        <!-- jvectormap CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/jvectormap/jquery-jvectormap-2.0.3.css">
        <!-- normalize CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/data-table/bootstrap-table.css">
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/data-table/bootstrap-editable.css">
        <!-- normalize CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/normalize.css">
        <!-- charts CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/c3.min.css">
        <!-- style CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/style.css">
        <!-- responsive CSS
    		============================================ -->
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/responsive.css">
        <!-- modernizr JS
    		============================================ -->
        <script src="<?php echo BASE_URL; ?>assets/admin/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body class="materialdesign">
        <!-- login Start-->
        <div class="login-form-area mg-t-30 mg-b-40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <form id="adminpro-form" class="adminpro-form" method="POST">
                        <div class="col-lg-4">
                            <div class="login-bg">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="logo">
                                            <a href="#"><img src="<?php echo BASE_URL; ?>/assets/admin/img/logo.png" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="login-title">
                                            <h1>LOGIN</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="login-input-head">
                                            <p>E-mail</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="login-input-area">
                                            <input type="text" name="adminUsername"/>
                                            <i class="fa fa-envelope login-user" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="login-input-head">
                                            <p>Senha</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="login-input-area">
                                            <input type="password" name="adminPassword"/>
                                            <i class="fa fa-lock login-user"></i>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="forgot-password">
                                                    <a href="#">Trocar Senha</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="login-keep-me">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="remember" checked><i></i>Lembrar
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-8">
                                        <div class="login-button-pro">
                                            <button type="submit" class="login-button login-button-lg">Entrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-4"></div>
                </div>
            </div>
        </div>
        <!-- login End-->
    </div>
</div>
<!-- Footer Start-->
<div class="footer-copyright-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-copy-right">
                    <p>Copyright &#169; 2018 MeteorCode Todos os direitos Reservados. Desenvolvido por <a href="https://meteorcode.dev">Meteor Code</a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End-->
<!-- jquery
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/vendor/jquery-1.11.3.min.js"></script>
<!-- bootstrap JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/bootstrap.min.js"></script>
<!-- meanmenu JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.meanmenu.js"></script>
<!-- mCustomScrollbar JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- sticky JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.sticky.js"></script>
<!-- scrollUp JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.scrollUp.min.js"></script>
<!-- scrollUp JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/wow/wow.min.js"></script>
<!-- counterup JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/counterup/jquery.counterup.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/counterup/waypoints.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/counterup/counterup-active.js"></script>
<!-- jvectormap JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/jvectormap/jvectormap-active.js"></script>
<!-- peity JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/peity/jquery.peity.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/peity/peity-active.js"></script>
<!-- sparkline JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/sparkline/sparkline-active.js"></script>
<!-- flot JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.spline.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.resize.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.pie.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.symbol.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.time.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/flot/dashtwo-flot-active.js"></script>
<!-- data table JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/tableExport.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/data-table-active.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table-editable.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-editable.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table-resizable.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/colResizable-1.5.source.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table-export.js"></script>
<!-- main JS
============================================ -->
<script src="<?php echo BASE_URL; ?>assets/admin/js/main.js"></script>
<script src="<?php echo BASE_URL; ?>assets/admin/js/cepSearch.js"></script>
</body>

</html>
