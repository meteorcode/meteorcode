<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dashboard MeteorCode</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo BASE_URL; ?>assets/admin/img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/font-awesome.min.css">
    <!-- adminpro icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/jquery.mCustomScrollbar.min.css">
    <!-- form CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/form.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/animate.css">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/data-table/bootstrap-editable.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/normalize.css">
    <!-- charts CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/c3.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/admin/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/vendor/modernizr-2.8.3.min.js"></script>

</head>

<body class="materialdesign">
  <!--[if lt IE 8]>
          <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
  <!-- Header top area start-->
  <div class="wrapper-pro">
      <div class="left-sidebar-pro">
          <nav id="sidebar">
              <div class="sidebar-header">
                  <a href="#"><img src="<? echo BASE_URL; ?>/assets/admin/img/logometeor.png" alt="" />
                  </a>
                  <h3 style="color: #FFFFFF !important;">Gustavo</h3>
                  <p style="color: #FFFFFF !important;">Developer</p>
                  <strong style="color: #FFFFFF !important;">MC</strong>
              </div>
              <div class="left-custom-menu-adp-wrap">
                  <ul class="nav navbar-nav left-sidebar-menu-pro">
                      <li class="nav-item">
                          <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-home"></i> <span class="mini-dn">Home</span></a>
                      </li>
                      <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-envelope"></i> <span class="mini-dn">Configurações</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                          <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                              <a href="compose-mail.html" class="dropdown-item">Home</a>
                              <a href="view-mail.html" class="dropdown-item">Sobre Nós</a>
                              <a href="inbox.html" class="dropdown-item">Email</a>
                              <a href="inbox.html" class="dropdown-item">Banco</a>
                          </div>
                      </li>
                      <li class="nav-item"><a href="<?php echo BASE_URL; ?>admin/projects.php" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-flask"></i> <span class="mini-dn">Projetos</span></a>
                      </li>
                      <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-pie-chart"></i> <span class="mini-dn">Serviços</span></a>
                      </li>
                      <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-bar-chart-o"></i> <span class="mini-dn">Contatos</span></a>
                      </li>
                      <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-edit"></i> <span class="mini-dn">Depoimentos</span></a>
                      </li>
                  </ul>
              </div>
          </nav>
      </div>
      <!-- Header top area start-->
      <div class="content-inner-all">
          <div class="header-top-area">
              <div class="fixed-header-top">
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col-lg-1 col-md-6 col-sm-6 col-xs-12">
                              <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                  <i class="fa fa-bars"></i>
                              </button>
                              <div class="admin-logo logo-wrap-pro">
                                  <a href="#"><img src="img/logo/log.png" alt="" />
                                  </a>
                              </div>
                          </div>
                          <div class="col-lg-6 col-md-1 col-sm-1 col-xs-12">

                          </div>
                          <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                              <div class="header-right-info">
                                  <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                      <li class="nav-item dropdown">
                                          <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span class="adminpro-icon adminpro-chat-pro"></span><span class="indicator-ms"></span></a>
                                          <div role="menu" class="author-message-top dropdown-menu animated flipInX">
                                              <div class="message-single-top">
                                                  <h1>Message</h1>
                                              </div>
                                              <ul class="message-menu">
                                                  <li>
                                                      <a href="#">
                                                          <div class="message-img">
                                                              <img src="img/message/1.jpg" alt="">
                                                          </div>
                                                          <div class="message-content">
                                                              <span class="message-date">16 Sept</span>
                                                              <h2>Advanda Cro</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                                  <li>
                                                      <a href="#">
                                                          <div class="message-img">
                                                              <img src="img/message/4.jpg" alt="">
                                                          </div>
                                                          <div class="message-content">
                                                              <span class="message-date">16 Sept</span>
                                                              <h2>Sulaiman din</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                                  <li>
                                                      <a href="#">
                                                          <div class="message-img">
                                                              <img src="img/message/3.jpg" alt="">
                                                          </div>
                                                          <div class="message-content">
                                                              <span class="message-date">16 Sept</span>
                                                              <h2>Victor Jara</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                                  <li>
                                                      <a href="#">
                                                          <div class="message-img">
                                                              <img src="img/message/2.jpg" alt="">
                                                          </div>
                                                          <div class="message-content">
                                                              <span class="message-date">16 Sept</span>
                                                              <h2>Victor Jara</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                              </ul>
                                              <div class="message-view">
                                                  <a href="#">View All Messages</a>
                                              </div>
                                          </div>
                                      </li>
                                      <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa fa-bell-o" aria-hidden="true"></i><span class="indicator-nt"></span></a>
                                          <div role="menu" class="notification-author dropdown-menu animated flipInX">
                                              <div class="notification-single-top">
                                                  <h1>Notifications</h1>
                                              </div>
                                              <ul class="notification-menu">
                                                  <li>
                                                      <a href="#">
                                                          <div class="notification-icon">
                                                              <span class="adminpro-icon adminpro-checked-pro"></span>
                                                          </div>
                                                          <div class="notification-content">
                                                              <span class="notification-date">16 Sept</span>
                                                              <h2>Advanda Cro</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                                  <li>
                                                      <a href="#">
                                                          <div class="notification-icon">
                                                              <span class="adminpro-icon adminpro-cloud-computing-down"></span>
                                                          </div>
                                                          <div class="notification-content">
                                                              <span class="notification-date">16 Sept</span>
                                                              <h2>Sulaiman din</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                                  <li>
                                                      <a href="#">
                                                          <div class="notification-icon">
                                                              <span class="adminpro-icon adminpro-shield"></span>
                                                          </div>
                                                          <div class="notification-content">
                                                              <span class="notification-date">16 Sept</span>
                                                              <h2>Victor Jara</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                                  <li>
                                                      <a href="#">
                                                          <div class="notification-icon">
                                                              <span class="adminpro-icon adminpro-analytics-arrow"></span>
                                                          </div>
                                                          <div class="notification-content">
                                                              <span class="notification-date">16 Sept</span>
                                                              <h2>Victor Jara</h2>
                                                              <p>Please done this project as soon possible.</p>
                                                          </div>
                                                      </a>
                                                  </li>
                                              </ul>
                                              <div class="notification-view">
                                                  <a href="#">View All Notification</a>
                                              </div>
                                          </div>
                                      </li>
                                      <li class="nav-item">
                                          <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                              <span class="adminpro-icon adminpro-user-rounded header-riht-inf"></span>
                                              <span class="admin-name">Advanda Cro</span>
                                              <span class="author-project-icon adminpro-icon adminpro-down-arrow"></span>
                                          </a>
                                          <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated flipInX">
                                              <li><a href="#"><span class="adminpro-icon adminpro-user-rounded author-log-ic"></span>Meu Perfil</a>
                                              </li>
                                              <li><a href="#"><span class="adminpro-icon adminpro-money author-log-ic"></span>Alterar Senha</a>
                                              </li>
                                              <li><a href="<?php echo BASE_URL;?>admin/logout"><span class="adminpro-icon adminpro-locked author-log-ic"></span>Sair</a>
                                              </li>
                                          </ul>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Header top area end-->
          <!-- Breadcome start-->
          <div class="breadcome-area mg-b-30 small-dn">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                              <div class="row">
                                  <div class="col-lg-6">
                                      <div class="breadcome-heading">
                                          <form role="search" class="">
                      <input type="text" placeholder="Search..." class="form-control">
                      <a href=""><i class="fa fa-search"></i></a>
                    </form>
                                      </div>
                                  </div>
                                  <div class="col-lg-6">
                                      <ul class="breadcome-menu">
                                          <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                          </li>
                                          <li><span class="bread-blod">Dashboard 2</span>
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Breadcome End-->
          <!-- Mobile Menu start -->
          <div class="mobile-menu-area">
              <div class="container">
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="mobile-menu">
                              <nav id="dropdown">
                                  <ul class="mobile-menu-nav">
                                      <li><a data-toggle="collapse" data-target="#Charts" href="#">Home <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul class="collapse dropdown-header-top">
                                              <li><a href="dashboard.html">Dashboard v.1</a>
                                              </li>
                                              <li><a href="dashboard-2.html">Dashboard v.2</a>
                                              </li>
                                              <li><a href="analytics.html">Analytics</a>
                                              </li>
                                              <li><a href="widgets.html">Widgets</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#demo" href="#">Mailbox <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="demo" class="collapse dropdown-header-top">
                                              <li><a href="inbox.html">Inbox</a>
                                              </li>
                                              <li><a href="view-mail.html">View Mail</a>
                                              </li>
                                              <li><a href="compose-mail.html">Compose Mail</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#others" href="#">Miscellaneous <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="others" class="collapse dropdown-header-top">
                                              <li><a href="profile.html">Profile</a>
                                              </li>
                                              <li><a href="contact-client.html">Contact Client</a>
                                              </li>
                                              <li><a href="contact-client-v.1.html">Contact Client v.1</a>
                                              </li>
                                              <li><a href="project-list.html">Project List</a>
                                              </li>
                                              <li><a href="project-details.html">Project Details</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#Miscellaneousmob" href="#">Interface <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="Miscellaneousmob" class="collapse dropdown-header-top">
                                              <li><a href="google-map.html">Google Map</a>
                                              </li>
                                              <li><a href="data-maps.html">Data Maps</a>
                                              </li>
                                              <li><a href="pdf-viewer.html">Pdf Viewer</a>
                                              </li>
                                              <li><a href="x-editable.html">X-Editable</a>
                                              </li>
                                              <li><a href="code-editor.html">Code Editor</a>
                                              </li>
                                              <li><a href="tree-view.html">Tree View</a>
                                              </li>
                                              <li><a href="preloader.html">Preloader</a>
                                              </li>
                                              <li><a href="images-cropper.html">Images Cropper</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#Chartsmob" href="#">Charts <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="Chartsmob" class="collapse dropdown-header-top">
                                              <li><a href="bar-charts.html">Bar Charts</a>
                                              </li>
                                              <li><a href="line-charts.html">Line Charts</a>
                                              </li>
                                              <li><a href="area-charts.html">Area Charts</a>
                                              </li>
                                              <li><a href="rounded-chart.html">Rounded Charts</a>
                                              </li>
                                              <li><a href="c3.html">C3 Charts</a>
                                              </li>
                                              <li><a href="sparkline.html">Sparkline Charts</a>
                                              </li>
                                              <li><a href="peity.html">Peity Charts</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Tables <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="Tablesmob" class="collapse dropdown-header-top">
                                              <li><a href="static-table.html">Static Table</a>
                                              </li>
                                              <li><a href="data-table.html">Data Table</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#formsmob" href="#">Forms <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="formsmob" class="collapse dropdown-header-top">
                                              <li><a href="basic-form-element.html">Basic Form Elements</a>
                                              </li>
                                              <li><a href="advance-form-element.html">Advanced Form Elements</a>
                                              </li>
                                              <li><a href="password-meter.html">Password Meter</a>
                                              </li>
                                              <li><a href="multi-upload.html">Multi Upload</a>
                                              </li>
                                              <li><a href="tinymc.html">Text Editor</a>
                                              </li>
                                              <li><a href="dual-list-box.html">Dual List Box</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#Appviewsmob" href="#">App views <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="Appviewsmob" class="collapse dropdown-header-top">
                                              <li><a href="basic-form-element.html">Basic Form Elements</a>
                                              </li>
                                              <li><a href="advance-form-element.html">Advanced Form Elements</a>
                                              </li>
                                              <li><a href="password-meter.html">Password Meter</a>
                                              </li>
                                              <li><a href="multi-upload.html">Multi Upload</a>
                                              </li>
                                              <li><a href="tinymc.html">Text Editor</a>
                                              </li>
                                              <li><a href="dual-list-box.html">Dual List Box</a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li><a data-toggle="collapse" data-target="#Pagemob" href="#">Pages <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                          <ul id="Pagemob" class="collapse dropdown-header-top">
                                              <li><a href="login.html">Login</a>
                                              </li>
                                              <li><a href="register.html">Register</a>
                                              </li>
                                              <li><a href="captcha.html">Captcha</a>
                                              </li>
                                              <li><a href="checkout.html">Checkout</a>
                                              </li>
                                              <li><a href="contact.html">Contacts</a>
                                              </li>
                                              <li><a href="review.html">Review</a>
                                              </li>
                                              <li><a href="order.html">Order</a>
                                              </li>
                                              <li><a href="comment.html">Comment</a>
                                              </li>
                                          </ul>
                                      </li>
                                  </ul>
                              </nav>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Mobile Menu end -->
          <!-- Breadcome start-->
          <div class="breadcome-area des-none mg-b-30">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                              <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="breadcome-heading">
                                          <form role="search" class="">
                      <input type="text" placeholder="Search..." class="form-control">
                      <a href=""><i class="fa fa-search"></i></a>
                    </form>
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <ul class="breadcome-menu">
                                          <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                          </li>
                                          <li><span class="bread-blod">Dashboard</span>
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Breadcome End-->

    <?php $this->loadViewInTemplate($viewName,$viewData); ?>

    <!-- jquery
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/vendor/jquery-1.11.3.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets\js\ajaxRequest.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/bootstrap.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.meanmenu.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/jquery.scrollUp.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/wow/wow.min.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/counterup/jquery.counterup.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/counterup/waypoints.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/counterup/counterup-active.js"></script>
    <!-- jvectormap JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/jvectormap/jvectormap-active.js"></script>
    <!-- peity JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/peity/jquery.peity.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/peity/peity-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.symbol.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/jquery.flot.time.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/flot/dashtwo-flot-active.js"></script>
    <!-- data table JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/tableExport.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/data-table-active.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table-editable.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-editable.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table-resizable.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/colResizable-1.5.source.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/admin/js/data-table/bootstrap-table-export.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/admin/js/main.js"></script>
    <!-- Jquery Mask JS
    ============================================ -->
    <script src="<?php echo BASE_URL; ?>assets/vendor/jquery.mask.js"></script>
</body>

</html>
