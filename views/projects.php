<!-- Register Start -->
<div class="login-form-area mg-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12"></div>
            <form id="adminpro-register-form" method="POST" class="adminpro-form">
                <div class="col-lg-12">
                    <div class="login-bg">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="logo">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-title">
                                    <h1><strong>Formulario cadastro de Projetos</strong></h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group-inner">
                                    <label>Titulo</label>
                                    <input type="text" name="name" class="form-control" placeholder="Nome completo">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group-inner">
                                    <label>Descrição</label>
                                    <textarea type="text" name="descricao" class="form-control" placeholder="Descrição"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="login-button-pro">
                                    <button type="submit" class="login-button login-button-lg">Adicionar Projeto</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-lg-3"></div>
        </div>
    </div>
</div>
<!-- Register End-->

<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline13-list shadow-reset">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Projects <span class="table-project-n">Data</span> Table</h1>
                            <div class="sparkline13-outline-icon">
                                <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                <span><i class="fa fa-wrench"></i></span>
                                <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                                <select class="form-control">
                                    <option value="">Export Basic</option>
                                    <option value="all">Export All</option>
                                    <option value="selected">Export Selected</option>
                                </select>
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th data-field="name" data-editable="false">Titulo</th>
                                        <th data-field="email" data-editable="false">Descrição</th>
                                        <!-- <th data-field="phone" data-editable="false">Imagem</th> -->
                                        <th data-field="action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($projects as $value): ?>
                                    <tr>
                                        <td><?php echo $value['titulo']; ?></td>
                                        <td><?php echo $value['descricao']; ?></td>
                                        <!-- <td><?php //echo $value['img']; ?></td> -->
                                        <td class="datatable-ct"><i class="fa fa-check"></i></td>
                                    </tr>
                                  <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Static Table End