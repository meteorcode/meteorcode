<!-- Register Start-->
<div class="login-form-area mg-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12"></div>
            <form id="adminpro-register-form" method="POST" class="adminpro-form">
                <div class="col-lg-12">
                    <div class="login-bg">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="logo">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-title">
                                    <h1><strong>Formulario de registro de clientes</strong></h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group-inner">
                                    <label>Nome Completo</label>
                                    <input type="text" name="name" class="form-control" placeholder="Nome completo">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group-inner">
                                    <label>Endereço de Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="E-mail">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group-inner">
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="fantasy-name" class="form-control" placeholder="Opcional">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group-inner">
                                    <label>Razão Social</label>
                                    <input type="text" name="social-reason" class="form-control" placeholder="Opcional">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group-inner">
                                    <label>Telefone</label>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone" data-mask="(00) 0 0000-0000">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group-inner">
                                    <label>CPF / CNPJ</label>
                                    <input type="text" name="cpf" class="form-control" placeholder="CPF / CNPJ" data-mask="000.000.000-00">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group-inner">
                                    <label>CEP</label>
                                    <input id="cep" type="text" name="cep" class="form-control" placeholder="CEP" data-mask="00000000">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group-inner">
                                    <label>Endereço</label>
                                    <input id="address" type="text" name="adress" class="form-control" placeholder="Endereço">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group-inner">
                                    <label>Cidade</label>
                                    <input id="city" type="text" name="city" class="form-control" placeholder="Cidade">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group-inner">
                                    <label>Estado</label>
                                    <input id="uf" type="text" name="uf" class="form-control" placeholder="Estado">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group-inner">
                                    <label>Data de Nascimento</label>
                                    <input type="text" name="birthday" class="form-control" placeholder="Data de Nascimento" data-mask="00/00/0000">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="login-button-pro">
                                    <button type="submit" class="login-button login-button-lg">Adicionar cliente</button>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
            <div class="col-lg-3"></div>
        </div>
    </div>
</div>
<!-- Register End-->

<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline13-list shadow-reset">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Projects <span class="table-project-n">Data</span> Table</h1>
                            <div class="sparkline13-outline-icon">
                                <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                <span><i class="fa fa-wrench"></i></span>
                                <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                                <select class="form-control">
                                    <option value="">Export Basic</option>
                                    <option value="all">Export All</option>
                                    <option value="selected">Export Selected</option>
                                </select>
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th data-field="name" data-editable="false">Nome</th>
                                        <th data-field="email" data-editable="false">Email</th>
                                        <th data-field="phone" data-editable="false">Telefone</th>
                                        <th data-field="cpf" data-editable="false">CPF</th>

                                        <th data-field="task" data-editable="false">UF</th>
                                        <th data-field="date" data-editable="false">CEP</th>
                                        <th data-field="price" data-editable="false">Cidade</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($users as $value): ?>
                                    <tr>
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo $value['email']; ?></td>
                                        <td><?php echo $value['phone']; ?></td>
                                        <td><?php echo $value['cpf']; ?></td>
                                        <td><?php echo $value['uf']; ?></td>
                                        <td><?php echo $value['cep']; ?></td>
                                        <td><?php echo $value['cidade']; ?></td>
                                        <td class="datatable-ct"><i class="fa fa-check"></i></td>
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Static Table End -->
