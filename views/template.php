<!DOCTYPE html>
<html lang="pt-br">
  <head>
  <title>Meteor Code</title>
    <meta charset="UTF-8">
    <meta name="description" content="Software House Digital que tem como foco oferecer soluções digitais para empresas de pequeno e médio porte.Desenvolvimento de Sites, Aplicativos e sistemas Web">
    <meta name="keywords" content="meteorcode, meteor, code,código, software, software house, aplicativos, fabrica de aplicativo, website, criar website, desenvolvedor, programar, sistemas, site, loja virtual, e-commerce, tecnologia">
    <meta name="robots" content="">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="Portuguese">
    <meta name="generator" content="N/A">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?php echo BASE_URL;?>assets/images/core-img/favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/animate.css">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/aos.css">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/ionicons.min.css">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/icomoon.css">

    <!-- Responsive CSS -->
    <link href="<?php echo BASE_URL;?>assets/css/responsive/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/style.css">
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <?php $this->loadViewInTemplate($viewName,$viewData); ?>

    <script src="<?php echo BASE_URL.'assets/js/jquery-3.3.1.min.js'?>"></script>
    <script src="<?php echo BASE_URL.'assets/js/bootstrap.min.js'?>"></script>
</body>

</html>
