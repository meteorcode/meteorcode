
        <!-- income order visit user Start -->
        <div class="income-order-visit-user-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="income-dashone-total shadow-reset nt-mg-b-30">
                            <div class="income-title">
                                <div class="main-income-head">
                                    <h2>Receitas</h2>
                                    <div class="main-income-phara">
                                        <p>Mês</p>
                                    </div>
                                </div>
                            </div>
                            <div class="income-dashone-pro">
                                <div class="income-rate-total">
                                    <div class="price-adminpro-rate">
                                        <h3><span>R$</span><span class="counter">12.000</span></h3>
                                    </div>
                                </div>
                                <div class="income-range">
                                    <p>Total 6 Meses</p>
                                    <div class="price-adminpro-rate">
                                    <span class="income-percentange">
                                        <span>R$</span><span class="counter">50.000</span>
                                    </span>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="income-dashone-total shadow-reset nt-mg-b-30">
                            <div class="income-title">
                                <div class="main-income-head">
                                    <h2>Despesas</h2>
                                    <div class="main-income-phara order-cl">
                                        <p>Mês</p>
                                    </div>
                                </div>
                            </div>
                            <div class="income-dashone-pro">
                                <div class="income-rate-total">
                                    <div class="price-adminpro-rate">
                                        <h3><span>R$</span><span class="counter">5.000</span></h3>
                                    </div>
                                </div>
                                <div class="income-range">
                                    <p>Total 6 Meses</p>
                                    <div class="price-adminpro-rate">
                                    <span class="income-percentange" style="color: red !important;">
                                        <span>R$</span><span class="counter">70.000</span>
                                    </span>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="income-dashone-total shadow-reset nt-mg-b-30">
                            <div class="income-title">
                                <div class="main-income-head">
                                    <h2>Contas Atrasadas</h2>
                                    <div class="main-income-phara visitor-cl">
                                        <p>Semana</p>
                                    </div>
                                </div>
                            </div>
                            <div class="income-dashone-pro">
                                <div class="income-rate-total">
                                    <div class="price-adminpro-rate">
                                        <h3><span class="counter">5</span></h3>
                                    </div>
                                </div>
                                <div class="income-range visitor-cl">
                                    <p>Total 3 Meses</p>
                                    <span class="income-percentange" style="color: orangered !important;">30</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="income-dashone-total shadow-reset nt-mg-b-30">
                            <div class="income-title">
                                <div class="main-income-head">
                                    <h2>Contas a Pagar</h2>
                                    <div class="main-income-phara low-value-cl">
                                        <p>Semana</p>
                                    </div>
                                </div>
                            </div>
                            <div class="income-dashone-pro">
                                <div class="income-rate-total">
                                    <div class="price-adminpro-rate">
                                        <h3><span class="counter">2</span></h3>
                                    </div>
                                </div>
                                <div class="income-range low-value-cl">
                                    <p>Total nos próximos 3 Meses</p>
                                    <span class="income-percentange" style="color: #ff2656 !important;">13</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- income order visit user End -->
        <div class="dashtwo-order-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="dashtwo-order-list shadow-reset">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="flot-chart flot-chart-dashtwo">
                                        <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="skill-content-3">
                                        <div class="skill">
                                            <div class="progress">
                                                <div class="lead-content">
                                                    <h3>2,346</h3>
                                                    <p>Total orders in period</p>
                                                </div>
                                                <div class="progress-bar wow fadeInLeft" data-progress="95%" style="width: 95%;" data-wow-duration="1.5s" data-wow-delay="1.2s"> <span>95%</span>
                                                </div>
                                            </div>
                                            <div class="progress">
                                                <div class="lead-content">
                                                    <h3>9,346</h3>
                                                    <p>Orders in last month</p>
                                                </div>
                                                <div class="progress-bar wow fadeInLeft" data-progress="85%" style="width: 85%;" data-wow-duration="1.5s" data-wow-delay="1.2s"><span>85%</span> </div>
                                            </div>
                                            <div class="progress progress-bt">
                                                <div class="lead-content">
                                                    <h3>2,34,600</h3>
                                                    <p>Monthly income from order</p>
                                                </div>
                                                <div class="progress-bar wow fadeInLeft" data-progress="93%" style="width: 93%;" data-wow-duration="1.5s" data-wow-delay="1.2s"><span>93%</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                    <div class="col-lg-12">
                        <div class="sparkline9-list shadow-reset mg-tb-30">
                            <div class="sparkline9-hd">
                                <div class="main-sparkline9-hd">
                                    <h1>User project list</h1>
                                    <div class="sparkline9-outline-icon">
                                        <span class="sparkline9-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                        <span><i class="fa fa-wrench"></i></span>
                                        <span class="sparkline9-collapse-close"><i class="fa fa-times"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="sparkline9-graph dashone-comment">
                                <div class="datatable-dashv1-list custom-datatable-overright dashtwo-project-list-data">
                                    <div id="toolbar1">
                                        <select class="form-control">
                                            <option value="">Export Basic</option>
                                            <option value="all">Export All</option>
                                            <option value="selected">Export Selected</option>
                                        </select>
                                    </div>
                                    <table id="table1" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-resizable="true" data-cookie="true" data-page-size="5" data-page-list="[5, 10, 15, 20, 25]" data-cookie-id-table="saveId" data-show-export="true">
                                        <thead>
                                            <tr>
                                                <th data-field="state" data-checkbox="true"></th>
                                                <th data-field="id">ID</th>
                                                <th data-field="status" data-editable="true">Status</th>
                                                <th data-field="date" data-editable="true">Date</th>
                                                <th data-field="phone" data-editable="true">User</th>
                                                <th data-field="company" data-editable="true">Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td>1</td>
                                                <td>Pending</td>
                                                <td>1:20pm</td>
                                                <td>John</td>
                                                <td>20%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>2</td>
                                                <td class="complete-project-dashtwo">Complete</td>
                                                <td>2:30pm</td>
                                                <td>khan</td>
                                                <td>25%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>3</td>
                                                <td>Complete</td>
                                                <td>4:50pm</td>
                                                <td>hok</td>
                                                <td>50%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>4</td>
                                                <td class="canceled-project-dashtwo">Canceled</td>
                                                <td>5:30pm</td>
                                                <td>dogi</td>
                                                <td>80%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>5</td>
                                                <td>Complete</td>
                                                <td>6:20pm</td>
                                                <td>joyi</td>
                                                <td>30%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>6</td>
                                                <td>Pending</td>
                                                <td>7:40pm</td>
                                                <td>joy</td>
                                                <td>40%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>7</td>
                                                <td>Complete</td>
                                                <td>9:40pm</td>
                                                <td>ulla</td>
                                                <td>90%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>8</td>
                                                <td>Pending</td>
                                                <td>11:20pm</td>
                                                <td>don</td>
                                                <td>95%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>9</td>
                                                <td>Pending</td>
                                                <td>5:20am</td>
                                                <td>dali</td>
                                                <td>85%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>10</td>
                                                <td>Complete</td>
                                                <td>6:20am</td>
                                                <td>poli</td>
                                                <td>77%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>11</td>
                                                <td>Pending</td>
                                                <td>7:30am</td>
                                                <td>payel</td>
                                                <td>55%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>12</td>
                                                <td>Complete</td>
                                                <td>8:22am</td>
                                                <td>flat</td>
                                                <td>36%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>13</td>
                                                <td>Pending</td>
                                                <td>9:25am</td>
                                                <td>alit</td>
                                                <td>49%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>14</td>
                                                <td>Complete</td>
                                                <td>10:30am</td>
                                                <td>win</td>
                                                <td>87%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>15</td>
                                                <td>Pending</td>
                                                <td>11:11am</td>
                                                <td>back</td>
                                                <td>97%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>16</td>
                                                <td>Canceled</td>
                                                <td>11:12am</td>
                                                <td>koy</td>
                                                <td>58%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>17</td>
                                                <td>Complete</td>
                                                <td>11:20pm</td>
                                                <td>khy</td>
                                                <td>22%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>18</td>
                                                <td>Pending</td>
                                                <td>12:20pm</td>
                                                <td>fat</td>
                                                <td>33%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>19</td>
                                                <td>Pending</td>
                                                <td>1:20pm</td>
                                                <td>pof</td>
                                                <td>65%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>20</td>
                                                <td>Complete</td>
                                                <td>11:20pm</td>
                                                <td>elit</td>
                                                <td>46%</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>21</td>
                                                <td>Pending</td>
                                                <td>5:20pm</td>
                                                <td>John</td>
                                                <td>57%</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

        <!-- Transitions Start-->
        <div class="transition-world-area">

        </div>
        <!-- Transitions End-->
    </div>
</div>
<!-- Footer Start-->
<div class="footer-copyright-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-copy-right">
                    <p>Copyright &#169; 2019 Todos os direitos reservados.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End-->
