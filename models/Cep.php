<?php
class Cep extends model{
  private $adress;
  private $country;
  private $city;
  private $district;

  public function __construct($cep){
    $uri = "https://viacep.com.br/ws/".$cep."/json";
    $json = file_get_contents($uri);
    $response = json_decode($json);
    $this->adress = $response->logradouro;
    $this->country = $response->uf;
    $this->city = $response->localidade;
    $this->district = $response->bairro;
  }
  public function getCity(){
    return $this->city;
  }
  public function getCountry(){
    return $this->country;
  }
  public function getAdress(){
    return $this->adress;
  }
  public function getDistrict(){
    return $this->district;
  }
}
