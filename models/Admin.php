<?php
class Admin extends model{
  public function adminAuth($uAdmin,$uPassword){
    $sql = $this->db->prepare("SELECT * FROM admin WHERE uAdmin = :uAdmin");
    $sql->bindValue(':uAdmin',$uAdmin);
    $sql->execute();
    if($sql->rowCount() > 0){
      $sql = $sql->fetch();
      if (password_verify($uPassword, $sql['uPassword'])) {
        $token = $this->createNumberToken();
        $_SESSION['adminToken'] = $token;
        $sql2 = $this->db->prepare("UPDATE admin SET aToken = :token WHERE id = :id");
        $sql2->bindValue(':token',$token);
        $sql2->bindValue(':id',$sql['id']);
        $sql2->execute();
        $_SESSION['adminLoginAuth'] = true;
      }
    }
  }
  public function genHash($pass){
    echo password_hash($pass,PASSWORD_DEFAULT);
  }
  public function createNumberToken(){
    return rand(100000,999999);
  }

  public function verifyAdminToken(){
    $sql = $this->db->query("SELECT * FROM admin WHERE id = 1");
    if ($sql->rowCount() > 0) {
      $sql = $sql->fetch();
      if ($sql['aToken'] == $_SESSION['adminToken']) {
        return true;
      }else {
        return false;
      }
    }
  }

  public function logout(){
    $_SESSION['adminLoginAuth'] = false;
  }
}

 ?>
